﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillHandler : MonoBehaviour {

	public int skillID;
	private float currentXP;
	private float nextXP;
	private int currentLevel;
	private float prevXP;
	private Player player;

	public Text xpText;
	public Text levelText;
	public Image progressBar;

	void Start () {

		player = GameObject.Find ("Settings").GetComponent <Player> ();

		currentLevel = 1;

		Player.Load ();

		if (skillID == 0) {

			currentXP = Player.communicationXP;
			if (currentXP < 50) {
				currentLevel = 1;
			}
		} else if (skillID == 1) {

			currentXP = Player.aimXP;
			if (currentXP < 50) {
				currentLevel = 1;
			}
		} else if (skillID == 2) {

			currentXP = Player.sprayControlXP;
			if (currentXP < 50) {
				currentLevel = 1;
			}
		}
	}

	void Update () {

		prevXP = nextXP - 50;
		nextXP = currentLevel * 50;

		xpText.text = (currentXP + " XP / " + nextXP + " XP");

		if (currentXP >= nextXP) {

			if (currentLevel < 100) {
				
				currentLevel += 1;
			}
		}

		levelText.text = "LEVEL " + currentLevel.ToString () + " / 100";

		progressBar.fillAmount = ((currentXP - prevXP) / 50);

		CalculateLevel ();
		Player.Save ();
	}

	void CalculateLevel () {

		if (skillID == 0) {

			currentXP = Player.communicationXP;
			if (currentXP < 50) {
				currentLevel = 1;
			}
		} else if (skillID == 1) {

			currentXP = Player.aimXP;
			if (currentXP < 50) {
				currentLevel = 1;
			}
		} else if (skillID == 2) {

			currentXP = Player.sprayControlXP;
			if (currentXP < 50) {
				currentLevel = 1;
			}
		}
	}
}