﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Country : MonoBehaviour {

	public string countryName;
	public Sprite countryFlag;

	private Image active;

	void Start () {

		countryName = transform.name;
		countryFlag = this.GetComponent <Image> ().sprite;

		active = transform.GetChild (0).GetComponent<Image> ();
	}

	void Update () {

		if (Player.countryName == countryName) {

			active.gameObject.SetActive (true);
		} else {

			active.gameObject.SetActive (false);
		}
	}

	public void SaveCountry () {

		Player.countryName = countryName;
	}
}