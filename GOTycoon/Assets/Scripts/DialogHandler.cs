﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogHandler : MonoBehaviour {

	public List<string> dialogSequence = new List<string> ();
	public int[] fonts;

	[HideInInspector]
	public int currentText = 0;
	private Text text;

	private GameObject introText;
	private GameObject createProfile;
	private Player player;
	private GameObject intro;
	private GameObject mainMenu;

	void Start () {

		text = GameObject.Find ("Intro Label").transform.GetComponent <Text> ();
		introText = GameObject.Find ("Intro Text");
		createProfile = GameObject.Find ("Create Profile");
		player = GameObject.Find ("Settings").GetComponent <Player> ();
		intro = GameObject.Find ("Intro");
		mainMenu = GameObject.Find ("Main Menu");
	}

	void Update () {

		if (dialogSequence.Count != 0) {
			
			text.text = dialogSequence [currentText];
			text.resizeTextMaxSize = fonts [currentText];
		} else {

			Debug.LogError (transform.name + "'s dialogSequence does not contain any strings.");
		}
			
		if (currentText == 4) {

			introText.SetActive (false);
			createProfile.SetActive (true);
		} else {

			introText.SetActive (true);
			createProfile.SetActive (false);
		}

		if (currentText == 7) {

			Player.cashGreater = 25;
			Player.cashDecimal = 0;
			Player.introComplete = true;
			Player.Save ();
			mainMenu.SetActive (true);
			intro.SetActive (false);
		}
	}

	public void AdvanceDialog () {

		currentText++;
	}
}
