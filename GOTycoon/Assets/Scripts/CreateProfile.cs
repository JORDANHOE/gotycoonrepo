﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateProfile : MonoBehaviour {

	private Text firstName;
	private Text alias;
	private Text lastName;
	private Text profileName;
	private Text country;
	private Button advanceButton;

	private bool profileReady;

	void Start () {

		firstName = GameObject.Find ("FN Text").GetComponent <Text> ();
		alias = GameObject.Find ("A Text").GetComponent <Text> ();
		lastName = GameObject.Find ("LN Text").GetComponent <Text> ();
		profileName = GameObject.Find ("CreateProfile Name").GetComponent <Text> ();
		country = GameObject.Find ("CreateProfile Country").GetComponent <Text> ();
		advanceButton = GameObject.Find ("Advance Button").GetComponent <Button> ();
	}

	void Update () {

		Player.firstName = firstName.text;
		Player.alias = alias.text;
		Player.lastName = lastName.text;

		profileName.text = Player.firstName + " '" + Player.alias + "' " + Player.lastName;
		country.text = Player.countryName;

		if (Player.firstName != null && Player.alias != null && Player.lastName != null && Player.countryName != null) {

			profileReady = true;
		} else {

			profileReady = false;
		}

		if (profileReady) {

			advanceButton.interactable = true;
		} else {

			advanceButton.interactable = false;
		}
	}
}