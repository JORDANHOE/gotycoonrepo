﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;

[System.Serializable]
public class PlayerData {

	public bool boolIntro;
	public string stringFirst;
	public string stringAlias;
	public string stringLast;
	public string stringCountry;
	public int intCashGreater;
	public int intCashDecimal;
	public int xpCommunication;
	public int xpAim;
	public int xpSprayControl;
}

public class Player : MonoBehaviour {

	public static bool introComplete;
	public static string firstName;
	public static string alias;
	public static string lastName;
	public static string countryName;
	public static int cashGreater;
	public static int cashDecimal;
	public static int communicationXP;
	public static int aimXP;
	public static int sprayControlXP;

	private GameObject intro;
	private GameObject mainMenu;

	void Start () {

		Load ();

		intro = GameObject.Find ("Intro");
		mainMenu = GameObject.Find ("Main Menu");

		if (introComplete) {

			intro.SetActive (false);
			mainMenu.SetActive (true);
		} else {

			intro.SetActive (true);
			mainMenu.SetActive (false);
		}
	}

	void Update () {


		if (cashDecimal >= 100) {

			cashGreater += 1;
			cashDecimal -= 100;
		}
	}

	public static void Save () {

		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/playerInfo.dat");

		PlayerData data = new PlayerData ();
		data.boolIntro = introComplete;
		data.stringFirst = firstName;
		data.stringAlias = alias;
		data.stringLast = lastName;
		data.stringCountry = countryName;
		data.intCashGreater = cashGreater;
		data.intCashDecimal = cashDecimal;
		data.xpCommunication = communicationXP;
		data.xpAim = aimXP;
		data.xpSprayControl = sprayControlXP;

		bf.Serialize (file, data);
		file.Close ();
	}

	public static void Load () {

		if (File.Exists (Application.persistentDataPath + "/playerInfo.dat")) {

			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
			PlayerData data = (PlayerData)bf.Deserialize (file);

			file.Close ();

			introComplete = data.boolIntro;
			firstName = data.stringFirst;
			alias = data.stringAlias;
			lastName = data.stringLast;
			countryName = data.stringCountry;
			cashGreater = data.intCashGreater;
			cashDecimal = data.intCashDecimal;
			communicationXP = data.xpCommunication;
			aimXP = data.xpAim;
			sprayControlXP = data.xpSprayControl;
		}
	}

	public void DeleteSave () {

		if (File.Exists (Application.persistentDataPath + "/playerInfo.dat")) {

			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");
			PlayerData data = new PlayerData ();

			data.boolIntro = false;
			data.stringFirst = string.Empty;
			data.stringAlias = string.Empty;
			data.stringLast = string.Empty;
			data.stringCountry = string.Empty;
			data.intCashGreater = 0;
			data.intCashDecimal = 0;
			data.xpCommunication = 0;
			data.xpAim = 0;
			data.xpSprayControl = 0;

			bf.Serialize (file, data);
			file.Close ();
		}
	}
}
