﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

	private Text profileName;
	private Text profileCash;

	public static int menuID;

	void Start () {

		profileName = GameObject.Find ("ProfileName Text").GetComponent <Text> ();
		profileCash = GameObject.Find ("ProfileCash Text").GetComponent <Text> ();
	}

	void Update () {
	
		profileName.text = Player.alias;

		if (Player.cashDecimal >= 0 && Player.cashDecimal < 10) {
			
			profileCash.text = "$" + Player.cashGreater + "." + "0" + Player.cashDecimal;
		} else {

			profileCash.text = "$" + Player.cashGreater + Player.cashDecimal;
		}
	}

	public void ChangeMenu (int id) {

		menuID = id;
	}
}
